# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import argparse
import base64
import binascii
import os
import filecmp
import shutil

from Crypto import Random
from Crypto.Signature import pkcs1_15
from Crypto.Util import Counter
from Crypto.Util.Padding import pad, unpad
from Crypto.Cipher import DES3, PKCS1_OAEP, AES
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA3_256, SHA3_512


class EncryptionAlgorithm:

    @staticmethod
    def make_key_file(method, algorithm, key, init_vector, key_file):
        """Create a new file containing encription key and initialization vector

                Arguments:
                ----------
                method :    string, ['ECB', 'CBC', 'CFB', 'OFB', 'CTR']
                           chain method to be used in encryption , defaults to ECB

                algorithm : string [DES3/AES]
                           algorithm to be used in the encryption

                key : bytes
                      Key in byte format to be used in encryption

                init_vector : bytes
                              Initialization vector in byte format to be used in encryption for certain chain methods

                key_file :  string
                           path to file containing key, will create new file if it doesnt exist or if left out
               """

        with open(key_file, mode="w") as output:
            list = ["---BEGIN NOS CRYPTO DATA---\n",
                    "Description: " + "Secret key\n",
                    "Algorithm: " + algorithm + '\n',
                    "Method: " + method + '\n',
                    "Key: " + base64.b64encode(key).decode() + "\n",
                    "Initialization vector: " + base64.b64encode(init_vector).decode() + '\n',
                    "---END NOS CRYPTO DATA---" + '\n',
                    ]
            output.writelines(list)

    @staticmethod
    def get_key_file_data(key_file):

        with open(key_file, mode="r") as input:
            list = input.read().splitlines()
            key = None
            iv = None
            method = None
            for line in list:
                if "Key" in line:
                    key = line.split(" ")[1].lstrip()
                elif "Initialization vector" in line:
                    iv = line.split(" ")[2].lstrip()
                # print("Linija " +line )
                elif "Method" in line:
                    method = line.split(" ")[1].lstrip()
                    # print(method)
                elif (iv is not None) and (key is not None) and (method is not None):
                    break

        if (iv is None) and (key is None) and (method is None):
            raise ValueError("Invalid keyfile provided")

        # print("Kljuc {} IV {}".format(key,iv))
        return base64.b64decode(key), base64.b64decode(iv), method

    @staticmethod
    def _write_enc_data(algorithm, encrypted_file_name, data):
        with open(encrypted_file_name, mode="w") as output:
            list = ["---BEGIN NOS CRYPTO DATA---\n",
                    "Description: " + "Crypted file\n",
                    "Method: " + algorithm + '\n',
                    "File name: " + encrypted_file_name + "\n",
                    "Data: " + base64.b64encode(data).decode() + '\n',
                    "---END NOS CRYPTO DATA---" + '\n',
                    ]
            output.writelines(list)

    @staticmethod
    def _get_enc_data(encrypted_file_name):
        with open(encrypted_file_name, mode="r") as input:
            list = input.read().splitlines()
            enc_data = None

            for line in list:
                if "Data" in line:
                    enc_data = line.split(" ")[1].lstrip()
                    break
        return base64.b64decode(enc_data)


class SimWrapper(EncryptionAlgorithm):
    """ Initialize SIMWrapperobject function

    Arguments:
    ----------
    algorithm : string [DES3/AES]
                algorithm to be used in the encryption

    key_file :  string
                path to file containing key, will create new file if it doesnt exist or if left out

    method :    string, ['ECB', 'CBC', 'CFB', 'OFB', 'CTR']
                chain method to be used in encryption , defaults to ECB

    overwrite : boolean
                if true will overwrite existing key file of the same name

    """

    def __init__(self, algorithm: str, key_file=None, method="ECB", overwrite=False):

        self.algorithm = algorithm
        if algorithm == "DES3":
            self.module = DES3
        elif algorithm == "AES":
            self.module = AES
        else:
            raise ValueError("Unsupported algorithm")

        self.Chain_Mode_Translate = {
            "ECB": AES.MODE_ECB,
            "CBC": AES.MODE_CBC,
            "CFB": AES.MODE_CFB,
            "OFB": AES.MODE_OFB,
            "CTR": AES.MODE_CTR,
        }
        self.key_file = key_file
        if not os.path.exists(key_file) or overwrite:
            secret_key, init_vector = Random.new().read(24), Random.new().read(self.module.block_size)
            self.make_key_file(method, algorithm, secret_key, init_vector, key_file)

    def set_key_file(self, key_file: str):
        self.key_file = key_file

    def encrypt(self, data_file_name: str, encrypted_file_name: str):

        key, iv, method = self.get_key_file_data(self.key_file)

        cipher = None
        with open(data_file_name, mode="r") as input:
            message = input.read()
            # message = message.rjust(math.ceil(len(message) / 24) * 24)

        if method == "CTR":
            ctr = Counter.new(128, initial_value=int(binascii.hexlify(iv), 16))
            cipher = self.module.new(key, self.Chain_Mode_Translate[method], counter=ctr)
        elif method == "ECB":
            cipher = self.module.new(key, self.Chain_Mode_Translate[method])
        elif method == "CFB" or args.chain_method == "OFB":
            cipher = self.module.new(key, self.Chain_Mode_Translate[method], iv)

        msg_enc = cipher.encrypt(pad(message.encode("utf-8"), 32))
        self._write_enc_data(self.algorithm, encrypted_file_name, msg_enc)

    def decrypt(self, encrypted_file_name: str, data_file_name: str):
        key, iv, method = self.get_key_file_data(self.key_file)
        enc_data = self._get_enc_data(encrypted_file_name)
        decipher = None
        if method == "CTR":
            ctr = Counter.new(128, initial_value=int(binascii.hexlify(iv), 16))
            decipher = self.module.new(key, self.Chain_Mode_Translate[method], counter=ctr)
        elif method == "ECB":
            decipher = self.module.new(key, self.Chain_Mode_Translate[method])
        elif method == "CFB" or args.chain_method == "OFB":
            decipher = self.module.new(key, self.Chain_Mode_Translate[method], iv)
        else:
            raise ValueError("Error unknown chain method")
        msg_dec = unpad(decipher.decrypt(enc_data), 32)

        with open(data_file_name, mode="w") as io:
            io.write(msg_dec.decode('utf-8'))


class Envelope():
    """ Envelope Class: class for making digital envelope
        Arguments:
        ----------
        wrp: SimWrapper
             Object containing wrapper for a symetrical encryption algorithm to be used in the process
        file_name : string
                    Name of file containing data to be encrypted
        output_file : string
                      Name of file where the envelope will be stored
    """

    def __init__(self, wrp: SimWrapper):
        self.wrp = wrp

    @staticmethod
    def _combine_files(envelope_key, output_file):
        with open(output_file, "a") as io:
            io.write("Envelope key: " + base64.b64encode(envelope_key).decode() + "\n")

    def _get_enc_sim_key(self, file_name):
        with open(file_name, "r") as io:
            list = io.read().splitlines()
            for line in list:
                if "Envelope key" in line:
                    return base64.b64decode(line.split(" ")[2])

    def make_envelope(self, file_name: str, output_file: str, public_key_file: str):
        self.wrp.encrypt(file_name, output_file)

        recipient_key = RSA.import_key(open(public_key_file, "rb").read())

        cipher_rsa = PKCS1_OAEP.new(recipient_key)  # Padding function
        enc_session_key = cipher_rsa.encrypt(open("keyfile.sim").read().encode('utf-8'))
        self._combine_files(enc_session_key, output_file)

    def decrypt_envelope(self, envelope_file: str, private_key_file: str, output_file: str):
        private_key = RSA.import_key(open(private_key_file).read())
        cipher_rsa = PKCS1_OAEP.new(private_key)
        session_key = cipher_rsa.decrypt(self._get_enc_sim_key(envelope_file))
        with open("decrypted_key.sim", mode="w") as io:
            io.write(session_key.decode())
        self.wrp.set_key_file("decrypted_key.sim")
        self.wrp.decrypt(envelope_file, output_file)
        os.system("rm decrypted_key.sim")  # remove temp key

    def verify_encryption(self, envelope_file: str, private_key_file: str, original_data_file: str):
        self.decrypt_envelope(envelope_file, private_key_file, "temp_decrypted")
        result = filecmp.cmp("temp_decrypted", original_data_file)
        os.system("rm temp_decrypted")
        return result


class Signature():
    """ OBSOLETE CLASS, Left only as a concept
    """

    def __init__(self, hash_algorithm: str):
        self.hash_module = None
        self.hash_algorithm = ""
        self.set_hash_algorithm(hash_algorithm)

    def set_hash_algorithm(self, hash_algorithm):
        self.hash_algorithm = hash_algorithm
        if hash_algorithm == "SHA3-256":
            hash_module = SHA3_256.new()
        elif hash_algorithm == "SHA3-512":
            hash_module = SHA3_512.new()
        else:
            raise NotImplementedError
        self.hash_module = hash_module

    def create_signature(self, private_key_file: str, data_file: str, output_file: str):
        private_key = RSA.import_key(open(private_key_file, "rb").read())

        self.hash_module.update(open(data_file).read().encode('utf-8'))
        cipher_rsa = PKCS1_OAEP.new(private_key)  # Padding function
        enc_digest = cipher_rsa.encrypt(self.hash_module.digest())
        self._write_signature(enc_digest, output_file)

    def _write_signature(self, enc_digest, output_file):
        with open(output_file, mode="w") as output:
            list = ["---BEGIN NOS CRYPTO DATA---\n",
                    "Description: " + "Digital signature\n",
                    "Method: " + "RSA" + '\n',
                    "Hash: " + self.hash_algorithm + "\n"
                                                     "File name: " + output_file + "\n",
                    "Signature: " + base64.b64encode(enc_digest).decode() + '\n',
                    "---END NOS CRYPTO DATA---" + '\n',
                    ]
            output.writelines(list)

    @staticmethod
    def _get_signature_data(signature_key_file):
        with open(signature_key_file, mode="r") as input:
            list = input.read().splitlines()
            hash = list[3].split(" ")[1]
            signature = list[5].split(" ")[1]

        return base64.b64decode(signature), hash

    def decrypt_signature_and_verify(self, public_key_file, signature_key_file, data_file):
        public_key = RSA.import_key(open(public_key_file).read())
        cipher_rsa = PKCS1_OAEP.new(public_key)
        signature, hash = self._get_signature_data(signature_key_file)
        self.set_hash_algorithm(hash)
        data_digest = cipher_rsa.decrypt(signature)  ## Pycryptodome cannot decrypt with public keys...
        self.hash_module.update(open(data_file, mode="r").read().encode("utf-8"))

        if data_digest == self.hash_module.digest:
            print("Signature authenticated and verified")
        else:
            print("Received data has been tampered with or corrupted")


def sign(data_file, priv_key, hash_algorithm, output_file):
    key = RSA.import_key(open(priv_key).read())
    signer = pkcs1_15.new(key)
    hash_module = None
    if hash_algorithm == "SHA3-256":
        hash_module = SHA3_256.new()
    elif hash_algorithm == "SHA3-512":
        hash_module = SHA3_512.new()
    else:
        raise NotImplementedError
    hash_module.update(open(data_file, mode="r").read().encode('utf-8'))
    with open(output_file, mode="wb") as io:
        io.write(base64.b64encode(signer.sign(hash_module)))


def verify(data_file, signature_file, hash_algorithm, pub_key_file):
    key = RSA.import_key(open(pub_key_file).read())
    signer = pkcs1_15.new(key)
    if hash_algorithm == "SHA3-512":
        digest = SHA3_512.new()
    elif hash_algorithm == "SHA3-256":
        digest = SHA3_256.new()
    else:
        raise NotImplementedError
    digest.update(open(data_file, mode="r").read().encode('utf-8'))

    signer.verify(digest, signature=base64.b64decode(open(signature_file, mode="rb").read()))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Choose parameters for encription")
    parser.add_argument('--filename', help="File which will be encrypted", default="podaci", dest="filename")
    parser.add_argument('--to_do', choices=['envelope', 'signature', 'stamp'],
                        help="What is the end goal of the encription? Defaults to stamp", default="stamp", dest="to_do")
    parser.add_argument('-s', choices=['AES', 'DES3'], default='AES', help='Simetrical encription algorithm to be used.'
                        , dest="sim_algorithm")
    parser.add_argument('--key-size', choices=[2048, 3072], default=2048, type=int,
                        help="Size of modulus member n"
                             "to be used in RSA encryption",
                        dest="key_size")
    parser.add_argument('-m', choices=['ECB', 'CBC', 'CFB', 'OFB', 'CTR'], default='ECB',
                        help='Chain method for simetrical encription. Defaults to ECB', dest='chain_method')
    parser.add_argument('-d', choices=["SHA3-256", "SHA3-512"], default="SHA3-256", help='Hashing algorithm for '
                                                                                         'making data digest',
                        dest="hash")
    parser.add_argument("--debug", help="If set will delete created key files and output files", default=False,
                        dest="debug", action='store_true')
    args = parser.parse_args()

    sim_algorithm = args.sim_algorithm
    goal = args.to_do

    # Setup symmetrical algorithm
    wrp = SimWrapper(sim_algorithm, "keyfile.sim", method=args.chain_method, overwrite=True)

    #
    # if filecmp.cmp(args.filename, "dekriptirano"):
    #    print("Algorithm verified")

    # Setup RSA
    key = RSA.generate(args.key_size)  # generira kljuc gdje je n velicine 2048 bita
    with open("rsa_key.bin", "wb") as file_out:
        file_out.write(key.export_key())

    with open("rsa_key_pub.bin", "wb") as file_out:
        file_out.write(key.publickey().export_key())

    if goal == "envelope":
        env = Envelope(wrp)
        env.make_envelope(args.filename, "envelope.x", "rsa_key_pub.bin")
        if env.verify_encryption("envelope.x", "rsa_key.bin", args.filename):
            print("Digital envelope verified successfully")
        else:
            print("Error in envelope encryption")
    elif goal == "signature":

        sign(args.filename, "rsa_key.bin", args.hash, "signature.x")
        try:
            verify(args.filename, "signature.x", args.hash, "rsa_key_pub.bin")
            print("Signature authenticated and verified")
        except (ValueError, TypeError):
            print("Received data has been tampered with or corrupted")

    else:  ## Stamp
        env = Envelope(wrp)
        env.make_envelope(args.filename, "envelope.x", "rsa_key_pub.bin")
        if env.verify_encryption("envelope.x", "rsa_key.bin", args.filename):
            print("Digital envelope verified successfully")
        else:
            print("Error in envelope encryption")
        sign("envelope.x", "rsa_key.bin", args.hash, "signed_envelope.x")
        try:
            verify("envelope.x", "signed_envelope.x", args.hash, "rsa_key_pub.bin")
            print("Signature authenticated and verified")
        except (ValueError, TypeError):
            print("Received data has been tampered with or corrupted")

    if args.debug:
        for filename in os.listdir():
            if ".py" not in filename and filename != args.filename:
                path = "./" + filename

                if os.path.isfile(path):
                    os.unlink(path)
                else:
                    shutil.rmtree(path)
