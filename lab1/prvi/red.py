from multiprocessing import Process, Queue
from time import sleep
from random import randint


# proces auto
def Auto(registracija, smjer, zah, odg, red_prolaza):
    # cekaj Z milisekundi
    Z = randint(100, 2000) / 1000
    sleep(Z)
    print("Automobil " + registracija + " smjer: " + smjer + " čeka na prelazak preko mosta")
    zah.put(registracija)
    odg.get()
    print("Automobil " + registracija + " popeo se na most")
    Y = randint(1000, 3000) / 1000
    sleep(Y)
    print("Automobil " + registracija + " " + smjer + " prešao most")
    red_prolaza.put(registracija)



# proces semafora
def Semafor(redovi):
    # odredivanje nasumicnog pocetnog smijera kretanja
    prolazni_smjer = randint(0, 1)
    if prolazni_smjer == 0:
        prolazni_smjer = "desno"
    else:
        prolazni_smjer = "lijevo"
    # radna petlja semafora funkcionira na sljedeci nacin:
    #   1.) provjerava red zahtjeva za smjer koji je trenutno prolazan u semaforu. Ako se nakupi 3 auta , stavlja ih u
    #       lokalnu listu autiju, inace dolazi do timeouta poslije X vremena i auti koji su stavili zahtjev do tad se
    #       stavljaju u lokalnu listu
    #   2.) salje odgovore u odgovarajuci red odgovora za aute koji su poslali zahtjeve
    #   3.) ceka na poruke u redu prolaska od autiju koji su na mostu
    #   4,) promjeni prolazan smjer

    while True:
        print("\nPROLAZAN SMJER " + prolazni_smjer)
        red_autiju = []
        X = randint(500, 1000) / 1000
        status = "OK"
        try:
            for i in range(0, 3):
                auto = redovi["zahtjevi"][prolazni_smjer].get(timeout=X)
                red_autiju.append(auto)
        except Exception as e:
            status = "TIMEOUT"
        finally:
            print(status + " " + prolazni_smjer + " broj autiju " + str(len(red_autiju)))

        for auto in red_autiju:
            redovi["odgovori"][prolazni_smjer].put("Prijeđi " + auto)

        for auto in red_autiju:
            redovi["prijelaz"].get()

        if prolazni_smjer == "desno":
            prolazni_smjer = "lijevo"
        else:
            prolazni_smjer = "desno"


# glavni program koji ce pokrenuti sve potrebne procese
if __name__ == '__main__':
    processCarArray = []
    # N procesa koje stvaramo
    N = randint(5, 100)

    # 5 reda poruka koji ce se koristiti u zadatku
    red_zahtjeva_lijevo = Queue(maxsize=100)
    red_zahtjeva_desno = Queue(maxsize=100)
    red_odgovora_lijevo = Queue(maxsize=100)
    red_odgovora_desno = Queue(maxsize=100)
    red_prolaza = Queue(maxsize=100)

    # struktura podataka za lakse snalazenje u redovima
    redovi = {"zahtjevi": {"desno": red_zahtjeva_desno,
                           "lijevo": red_zahtjeva_lijevo},
              "odgovori": {"desno": red_odgovora_desno,
                           "lijevo": red_odgovora_lijevo},
              "prijelaz": red_prolaza}

    # pokretanje procesa semafor
    sem = Process(target=Semafor, args=(redovi,))
    sem.start()

    # pokretanje N procesa auto
    for i in range(1, N):
        rega = "RI-" + str(i) + "-CK"
        smjer = randint(0, 1)
        if smjer == 0:
            smjer = "desno"
            odg = red_odgovora_desno
            zah = red_zahtjeva_desno
        else:
            smjer = "lijevo"
            odg = red_odgovora_lijevo
            zah = red_zahtjeva_lijevo
        p = Process(target=Auto, args=(rega, smjer, zah, odg, red_prolaza))
        p.start()
        processCarArray.append(p)

    # pricekaj sve procese auto da zavrse i terminiraj ih ako traju duze od 20 sekundi, da se ne zakrci cijeli sustav
    for process in processCarArray:

        process.join(20)

        # Proces nije zavrsio u zadanom vremenu, izbjegavanje deadlocka
        if process.exitcode is None:
            print("Process Auto je predugo trajao, timeout detektiran")
            process.terminate()

    sem.terminate()
    print("\n\n" + "#" * 41 + "\nKraj programa, svi auti izasli iz sustava\n" + "#" * 41)
