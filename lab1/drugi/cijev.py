from multiprocessing import Process, Pipe, Array
from json import dumps
from sys import argv
from ctypes import Structure, c_int
from copy import deepcopy
from random import randint
from time import sleep


# Struktura podataka za bazu podataka, pristup
class DB_entry(Structure):
    _fields_ = [("id", c_int), ("log_sat", c_int), ("broj_KO", c_int)]


# Prioritetni red koji sam sortira zahtjeve procesa po prioritetu
class PriorityQueue:

    def __init__(self):
        self.queue = []
        self.size = 0

    def add(self, item):

        inserted = False
        for i in range(self.size):
            if self.queue[i]["log_sat"] > item["log_sat"]:
                self.queue.insert(i, item)
                inserted = True
                break
            elif self.queue[i]["log_sat"] == item["log_sat"] and self.queue[i]["id"] > item["id"]:
                self.queue.insert(i, item)
                inserted = True
                break
        if not inserted:
            self.queue.append(item)
        self.size += 1

    def pop(self):
        self.size -= 1
        return self.queue.pop(0)

    def peek_first(self):
        return deepcopy(self.queue[0])

    def remove_id(self, id):

        found = False
        for i in range(self.size):
            if self.queue[i]["id"] == id:
                del self.queue[i]
                self.size -= 1
                found = True
                break
        return found

    def __repr__(self):
        return "reda zahtjeva: " + str(self.queue)


def kriticni_odsjecak(data_base, id, log_sat, ulazak):
    data_base[id].log_sat = log_sat

    data_base[id].broj_KO = ulazak
    # Ispisi stanje baze podataka
    print("\nProces {} printa stanje baze podataka {}. put".format(id, ulazak))
    print([("id: {} log_sat: {} broj_KO: {}".format(a.id, a.log_sat, a.broj_KO)) for a in data_base], end="")
    print("\n")
    # Spavaj X vremena
    X = randint(100, 2000) / 1000
    sleep(X)


# Proces koji trazi pristup u K.O
def node(conn, id, log_sat, proc_num, data_base):
    red_zahtjeva = PriorityQueue()
    ans = 0
    ulazak = 0
    u_obradi = False

    # Radna petlja procesa koja radi lamporta i ulazi u K.O
    while True:

        # Posalji zahtjev za ulazak u K.O ako nema vec 5 ulazaka ili ako vec nije poslao
        if ulazak < 5 and not u_obradi:
            request = {"id": id,
                       "type": "zahtjev",
                       "log_sat": log_sat,
                       }
            conn.send(request)
            u_obradi = True
            print("Proces {} salje poruku {}".format(id, dumps(request)))
            request.pop("type")
            red_zahtjeva.add(request)

        # Primanje poruke od drugih procesa u sustavu i tumacenje rezultata
        message = conn.recv()
        print("Proces {} dobio poruku {}".format(id, dumps(message)))
        log_sat = max(log_sat, message["log_sat"]) + 1
        type = message.pop("type")
        if type == "odgovor":
            ans += 1
        elif type == "zahtjev":

            response = {"id": message["id"],
                        "type": "odgovor",
                        "log_sat": log_sat, }
            print("Proces {} salje poruku {}".format(id, dumps(response)))
            conn.send(response)
            red_zahtjeva.add(message)
        elif type == "izlazak":
            red_zahtjeva.remove_id(message["id"])
        elif type == "umri":
            exit(0)
        # Ceka da primi N-1 odgovora od drugih procesa
        if ans == proc_num - 1:

            prvi = red_zahtjeva.peek_first()
            if prvi["id"] == id:
                ulazak += 1
                ans = 0
                u_obradi = False

                # Ulazak u K.O
                kriticni_odsjecak(data_base, id, log_sat, ulazak)

                # Odstrani vlastiti zahtjev iz lokalnog reda zahtjeva te posalji poruku ostalima
                izlazak = red_zahtjeva.pop()
                izlazak["type"] = "izlazak"
                conn.send(izlazak)
                print("Proces {} salje poruku {}".format(id, dumps(izlazak)))
        #print("Stanje procesa {} log_sat:{} {}".format(id,log_sat,red_zahtjeva))


if __name__ == '__main__':

    proc_num = 3
    try:
        proc_num = int(argv[1])
    except ValueError as e:
        print("Neispravan unos broja procesa, zavrsavam izvodenje")
        exit(1)

    middleman_pipe_end = []
    proc_list = []
    broadcast_type = ["zahtjev", "izlazak"]

    data_base = Array(DB_entry, size_or_initializer=proc_num, lock=False)
    index = 0
    exit_counter = 0

    # Napravi proc_num pipeova i procesa te ih dodaj u liste za kasniji rad s njima
    for i in range(proc_num):
        node_input_end, node_read_output_end = Pipe(duplex=True)
        middleman_pipe_end.append(node_input_end)
        data_base[i] = (i, 0, 0)
        p = Process(target=node, args=(node_read_output_end, i, 0, proc_num, data_base))
        proc_list.append(p)

    for p in proc_list:
        p.start()

    # Radna petlja posrednika, provjerava ima li koji cvor nesto za poslat i prosljeduje primljenje poruke
    while True:

        if middleman_pipe_end[index].poll():
            message = middleman_pipe_end[index].recv()
            type = message["type"]

            # Vrsta poruke koja se salje svim procesima
            if type in broadcast_type:
                if type == "izlazak":
                    exit_counter += 1
                for i in range(proc_num):
                    if i == message["id"]:
                        continue
                    middleman_pipe_end[i].send(message)
            # Proslijedi poruku odgovor prikladnom cvoru
            else:
                middleman_pipe_end[message["id"]].send(message)
        # Kad svi procesi zavrse posalji im poruku da zavrse s radom, pricekaj ih da zavrse i prekini program
        if exit_counter == proc_num * 5:
            print("\n\nPosrednik salje poruku procesima da zavrse\n\n")
            for conn in middleman_pipe_end:
                conn.send({"id": -1, "type": "umri", "log_sat": -1})
            for p in proc_list:
                p.join()
            break

        index = (index + 1) % proc_num
